#!/bin/sh

#echo "which ping:"
#which ping

echo "dpkg -S bin/ping:"
dpkg -S bin/ping

echo "ping -V:"
ping -V

echo "ping -c 1 -v 127.0.0.1:"
ping -c 1 -v 127.0.0.1
echo "return $?"

#echo "ping 127.0.0.1 56 1:"
#ping 127.0.0.1 56 1
#echo "return $?"

echo "ping -n 1 127.0.0.1:"
ping -n 1 127.0.0.1
echo "return $?"

echo "ifconfig -a:"
ifconfig -a

echo "ip a:"
ip a

